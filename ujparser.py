#!/usr/bin/env python3
import ujson

#example:
# json to dict: cfg = uj_r('config.txt')
# dict to json: uj_w(cfg, 'config.txt')


def uj_r(fname):
	with open('json/' + fname) as f:
		return ujson.load(f)

def uj_w(d, fname):
	with open('json/' + fname, 'w') as o_f:
		o_f.write(ujson.dumps(d))

