#!/usr/bin/python3
import time
import ujson
from ujparser import *
import RPi.GPIO as GPIO # Import Raspberry Pi GPIO library

GPIO.setwarnings(False) 
GPIO.setmode(GPIO.BOARD) # Use physical pin numbering

GPIO.setup(7, GPIO.OUT, initial=GPIO.LOW) 
GPIO.setup(8, GPIO.OUT, initial=GPIO.LOW) 
GPIO.setup(10, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(12, GPIO.OUT, initial=GPIO.LOW)

door_led = False
defrosting = False
compressor = False
compressor_relay = False
compressor_timer_on = False
fridge_fan = False
circ_fan =   False
fz_door_timeout = False

defrost_stop_time = int(time.time())
fg_open_door_time = int(time.time())
fz_open_door_time = int(time.time())
compressor_start_time = int(time.time())


def set_pins():
	if defrosting == True:
		GPIO.output(7, GPIO.HIGH) # Turn on
	if defrosting == False:
		GPIO.output(7, GPIO.LOW) # Turn off	
	
	if compressor == True:
		GPIO.output(8, GPIO.HIGH) # Turn on
	if compressor == False:
		GPIO.output(8, GPIO.LOW) # Turn off
		
	if fridge_fan == True:
		GPIO.output(10, GPIO.HIGH) # Turn on
	if fridge_fan == False:
		GPIO.output(10, GPIO.LOW) # Turn off
	
	if door_led == True:
		GPIO.output(12, GPIO.HIGH) # Turn on
	if door_led == False:
		GPIO.output(12, GPIO.LOW) # Turn off


######## Debug Console ########
def status_console():
	s = uj_r('status.json')
	fg_door = s['fridge_door']
	fz_door = s['freeze_door']
	compressor = s['compressor']
	fridge_fan = s['fridge_fan']
	circ_fan = s['circ_fan']
	defrosting = s['defrosting']

	print('''

	


	Freeze Temp  : {}
	Fridge Temp  : {}	
	Evaporator   : {}
	********************
	
	Door Fridge  : {}
	Door Freeze  : {}
	
	Fridge Fan   : {}	
	Compressor   : {}
	CompressorRel: {}

	Defrosting   : {}
	
	********************
	Time Now: {}
	'''.format(t_freeze, t_fridge,t_evaporator, fg_door, fz_door, fridge_fan, compressor, compressor_relay, defrosting, time_now))

def status_log():
	######## Write fridge status to file ########
	status = {'ct_fridge':t_fridge, 
			  'ct_freeze':t_freeze, 
			  'ct_evaporator':t_evaporator,
			  'fridge_door':fg_door, 
			  'freeze_door':fz_door, 
			  'defrosting':defrosting, 
			  'compressor':compressor, 
			  'fridge_fan':fridge_fan, 
			  'circ_fan':circ_fan}
	uj_w(status, 'status.json')


def correct_temp_timer():
	print('checking')
	if defrost_stop_time < time_now - 3:
		
		if t_freeze > cfg['temp_freeze'] + 3:
			return True


######## main loop ########
while True:
	time.sleep(1)
	time_now = int(time.time())
	
	######## Read config file ########
	cfg = uj_r('config.json') 
	
	######## Read sensors data
	s = uj_r('dummy.json') 
	fg_door = s['fridge_door']
	fz_door = s['freeze_door']
	t_fridge = s['t_fridge']
	t_freeze = s['t_freeze']
	t_evaporator = s['t_evaporator']
	
	status_log()
	status_console() #show debut console
	set_pins() #switch pins
	

	######## Compressor delay ########
	if compressor == True and compressor_relay == False and compressor_timer_on == False:
		compressor_start_time = time_now
		compressor_timer_on = True

		
	if compressor_timer_on == True and compressor_start_time < time_now - 4:
		compressor_relay = True
	
	if compressor == False:
		compressor_relay = False
		compressor_timer_on = False	


	######## check if Door closed ########
	if fg_door == True and fz_door == True:
		door_led = False
		fg_open_door_time = time_now
		fz_open_door_time = time_now
		fg_warning_door = False
		fz_warning_door = False
		fz_door_timeout = False
	
	######## if door open  ########
	if fg_door == False:
		door_led = True
		fridge_fan = False
		
		if fg_open_door_time < time_now - 5:
			fg_warning_door = True
			print('door warning')
		if fg_open_door_time < time_now - 15:
			print('beeping')
			
	if fz_door == False:
		door_led = True
		freeze_fan = False
		if fz_open_door_time < time_now - 5:
			fz_warning_door = True
			print('Freeze door waring')
			
		if fz_open_door_time < time_now - 12:
			print('freeze beeping')
			compressor = False
			defrosting = False
			fz_door_timeout = True
			defrost_start_time = time_now

	
	######## Normal Mode ######## 
	if defrosting == False:
		
		######## check defrost circle ########
		if defrost_stop_time < time_now - cfg['cycle_defrost'] and fz_door_timeout == False:
			defrosting = True
			defrost_start_time = time_now
		
		######## Restart Defrosting if incorrect temps ########  
		if defrost_stop_time < time_now - 3 and t_freeze  > cfg['temp_freeze'] + 5: 
			defrosting = True
			defrost_start_time = time_now
				
		######## Keep temps ########
		if t_freeze > cfg['temp_freeze'] and fz_door_timeout == False:
			compressor = True

		if t_freeze <= cfg['temp_freeze']:
			compressor = False

		if t_fridge > cfg['temp_fridge'] and fg_door == True:
			fridge_fan = True

		if t_fridge <= cfg['temp_fridge']:
			fridge_fan = False


	######## defrosting  ########
	if defrosting == True:
		compressor = False
		fridge_fan = False

		if defrost_start_time < time_now - cfg['time_defrost'] or t_evaporator >= cfg['exit_temp_defrost']:
			defrosting = False
			defrost_stop_time = time_now
######## the end
