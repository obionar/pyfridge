#!/usr/bin/env python3
import cgi
import ujson
from html_defaults import *
config_file = "config.txt"

form = cgi.FieldStorage()
set_temp_freeze = form.getfirst('temp_freeze', 'none')
set_temp_fridge = form.getfirst('temp_fridge', 'none')
set_temp_defrost = form.getfirst('temp_defrost', 'none')
set_exit_temp_defrost = form.getfirst('exit_temp_defrost','none')
set_cycle_defrost = form.getfirst('cycle_defrost', 'none')
set_time_defrost = form.getfirst('time_defrost', 'none')
set_delay_compressor = form.getfirst('delay_compressor', 'none')

cfg = {'temp_freeze': set_temp_freeze, 'temp_fridge': set_temp_fridge, 'temp_defrost': set_temp_defrost, 'exit_temp_defrost': set_exit_temp_defrost,'cycle_defrost': set_cycle_defrost,'time_defrost':set_time_defrost, 'delay_compressor':set_delay_compressor}

html_top()

with open(config_file, 'w') as f:
	f.write(ujson.dumps(cfg))

print('''
<h3>Done!</h3>
<div class="msg">
	<p>fridge: {}</p>
	<p>freeze: {}</p>
	<p>defrost: {}</p>
</div>
'<a href="config.py" class="btn btn-sm btn-a"><< Back</a>'
'''.format(set_temp_fridge, set_temp_freeze, set_temp_defrost, set_exit_temp_defrost, set_cycle_defrost, set_time_defrost, set_delay_compressor))


html_bottom()
