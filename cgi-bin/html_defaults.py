def html_top():
	print('''
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>fridge config</title>
		
		<link href="../min.css" rel="stylesheet" type="text/css">
		
		<style>
			h3 {
				border-bottom: 2px solid;
				margin-bottom: 10px;
				padding-top: 10px;
				padding-bottom: 5px;
			}
			form {
				
				
			}
			.c4{width:31%; margin-right: 1.5%;}

		</style>
	</head>
	<body>
		<nav class="nav" tabindex="-1" onclick="this.focus()">
			<div class="container">
				<a class="pagename current" href="/index.html">myFridge</a>

				<a href="/cgi-bin/config.py">Settings</a>
				<a href="/cgi-bin/test.py">Test Mode</a>
				<a href="#">Documentation</a>
			</div>
		</nav>
		<button class="btn-close btn btn-sm">×</button>
		<div class="container">
''')

def html_bottom():
	print('''
	</div>
	</body>
</html>
''')
