#!/usr/bin/env python3
import cgi
import ujson
from html_defaults import *

html_top()


compressor = '<div class="btn btn-sm btn-c">OFF</div>'
fridge_fan = '<div class="btn btn-sm btn-c">OFF</div>'
air_fan = '<div class="btn btn-sm btn-b">ON </div>'
defrost = '<div class="btn btn-sm btn-b">ON </div>'


#  <input type="radio" name="compressor" value="ON" checked> ON
#  <input type="radio" name="gender" value="off"> OFF




print('''

<div class="col c6">
<h3>Test Mode</h3>

<table class="table">
<form>
	<tr>
		<td>Compressor</td>
		<td>
			<input type="radio" name="compressor" value="ON"> ON |
			<input type="radio" name="compressor" value="off" checked> OFF
		</td>
	</tr>
	
	<tr>
		<td>Fridge Fan</td>
		<td>
			<input type="radio" name="fridge_fan" value="ON"> ON |
			<input type="radio" name="fridge_fan" value="off" checked> OFF
		</td>
	</tr>
	
	<tr>
		<td>Cerculation Fan</td>
		<td>
			<input type="radio" name="air_fan" value="ON"> ON |
			<input type="radio" name="air_fan" value="off" checked> OFF
		</td>
	</tr>
	
	<tr>
		<td>Defrost Heater</td>
		<td>
			<input type="radio" name="defrost" value="ON"> ON |
			<input type="radio" name="defrost" value="off" checked> OFF
		</td>
	</tr>

</form>

</table>
<input type = "submit" value = "Test Selected" />
</div>
'''.format(compressor, fridge_fan, air_fan, defrost))

html_bottom()
