#!/usr/bin/env python
import time
from ujparser import uj_r


while True:
	s = uj_r('status.json')
	door = s['door']
	compressor = s['compressor']
	fridge_fan = s['fridge_fan']
	circ_fan = s['circ_fan']
	defrosting = s['defrosting']

	print('''
	
	
	**********************
	Door Closed: {}
	Compressor:  {}
	Fridge Fan:  {}
	Circul Fan:  {}
	Defrosting:  {}
	**********************
	

	'''.format(door, compressor, fridge_fan, circ_fan, defrosting))
