#!/usr/bin/env python3
import ujson

def fridge_status():
	with open('json/status.json') as f:
		status = ujson.load(f)

	curr_temp_fridge = status['ct_fridge']
	curr_temp_freeze = status['ct_freeze']

	if status['fridge_door'] == True:
		door_closed = "Closed"
	if status['fridge_door'] == False:
		door_closed = "Open"
	if status['compressor'] == True:
		compressor_on = "ON"
	if status['compressor'] == False:
		compressor_on = "OFF"
	if status['fridge_fan'] == True:
		fridge_fan_on = "ON"
	if status['fridge_fan'] == False:
		fridge_fan_on = "OFF"
	if status['circ_fan'] == True:
		air_fan_on = 'ON'
	if status['circ_fan'] == False:
		air_fan_on = 'OFF'

	print('''
		<h3>Fridge Status</h3>
		<div class="fridge_status">
			
			<table class="table">
				<tr>
					<td>Fridge Temp Now</td><td>{}</td>
				</tr>
				<tr>
					<td>Freeze Temp Now</td><td>{}</td>
				</tr>


				<tr>
					<td> </td><td></td>
				</tr>

				<tr>
					<td>Compressor</td><td>{}</td>
				</tr>

				<tr>
					<td>Fridge Fan</td><td>{}</td>
				</tr>


				<tr>
					<td>Circulating Fan</td><td>{}</td>
				</tr>
				
				<tr>
					<td>Door</td><td>{}</td>
				</tr>


			</table>
		</div>


	'''.format(curr_temp_fridge, curr_temp_freeze,  fridge_fan_on, compressor_on, air_fan_on, door_closed))

