#!/usr/bin/env python3
import time
import cgi
import ujson
from html_defaults import *
from status import *

import cgitb
cgitb.enable()



#get forms data
form = cgi.FieldStorage()
set_temp_freeze = form.getfirst('temp_freeze', 'none')
set_temp_fridge = form.getfirst('temp_fridge', 'none')
set_temp_defrost = form.getfirst('temp_defrost', 'none')
set_exit_temp_defrost = form.getfirst('exit_temp_defrost','none')
set_cycle_defrost = form.getfirst('cycle_defrost', 'none')
set_time_defrost = form.getfirst('time_defrost', 'none')
set_delay_compressor = form.getfirst('delay_compressor', 'none')
test_mode = form.getfirst('mode', 'none')

if form:
	cfg = {'test_mode':int(test_mode), 'temp_freeze': int(set_temp_freeze), 'temp_fridge': int(set_temp_fridge), 'temp_defrost': int(set_temp_defrost), 'exit_temp_defrost': int(set_exit_temp_defrost),'cycle_defrost': int(set_cycle_defrost),'time_defrost': int(set_time_defrost), 'delay_compressor':int(set_delay_compressor)}
	with open('json/config.json', 'w') as f:
		f.write(ujson.dumps(cfg))

time.sleep(.1)
	
#forms prefill 
with open('json/config.json') as old_f: 
	old_data = ujson.load(old_f)
	o_temp_freeze = old_data['temp_freeze']
	o_temp_fridge = old_data['temp_fridge']
	o_temp_defrost = old_data['temp_defrost']
	o_exit_temp_defrost = old_data['exit_temp_defrost']
	o_cycle_defrost = old_data['cycle_defrost']
	o_time_defrost = old_data['time_defrost']
	o_delay_compressor = old_data['delay_compressor']
	o_test_mode = old_data['test_mode']



def config_table():
	print('''
	<h3>Fridge configuration</h3>
	{}
	<form action="config.py">
		<table class="table">
			<tr>
				<td>Fridge Temp</td>
				<td><input type="number" name="temp_fridge" min="1" max="10" value="{}" style="width: 60px;"></td>
				<td>C</td>
			</tr>
					
			<tr>
				<td>Freezer Temp</td>
				<td><input type="number" name="temp_freeze" min="-20" max="1" value="{}" style="width: 60px;"></td>
				<td>C</td>
			</tr>
					
			<tr>
				<td>Defrost Max Temp</td>
				<td><input type="number" name="temp_defrost" min="5" max="20" value="{}" style="width: 60px;"></td>
				<td>C</td>
			</tr>
					
			<tr>
				<td>Defrost Exit Temp</td>
				<td><input type="number" name="exit_temp_defrost" min="5" max="25" value="{}" style="width: 60px;"></td>
				<td>C</td>
			</tr>
					
			<tr>
				<td>Defrost Cycle Every:</td>
				<td><input type="number" name="cycle_defrost" min="5" max="120" value="{}" style="width: 60px;"></td>
				<td>Hours</td>
			</tr>
					
			<tr>
				<td>Defrost Time (Minutes)</td>
				<td><input type="number" name="time_defrost" min="1" max="100" value="{}" style="width: 60px;"></td>
				<td>Minutes</td>
			</tr>
			
			<tr>
				<td>Compressor Delay</td>
				<td><input type="number" name="delay_compressor" min="1" max="20" value="{}" style="width: 60px;"></td>
				<td>Minutes</td>
			</tr>
			
		</table>
		
	<h3>Mode Switch</h3>

	<input type="radio" name="mode" value="0" > Normal Mode </br>
	<input type="radio" name="mode" value="1" > Testing Mode </br>
	<input type = "submit" value = "Save Settings" />
</form>
	'''.format(mod_text, o_temp_fridge, o_temp_freeze, o_temp_defrost, o_exit_temp_defrost, o_cycle_defrost, o_time_defrost, o_delay_compressor))



print('Content-type: text/html\n')
html_top()

print('<div class="row">')
print('<div class="col c4 fridge_status">')
fridge_status()	


print('''



''')


print('</div>')
print('<div class="col c8 fridge_config ">')
if o_test_mode == True:
	mod_text = 'Testing mode enabled'
if o_test_mode == False:
	mod_text = 'Running in normal mode'
	config_table()

print('</div>')
print('</div>')

html_bottom()
